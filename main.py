import os, shutil


dirs = [('src', 'tests')]
for directories in dirs:
    code = [file[:file.index('.jl')] for file in os.listdir(directories[0]) if '.jl' in file]
    tests = [test_file[:test_file.index('.test.jl')] for test_file in os.listdir(directories[1]) if '.test.jl' in test_file]
    files = [file for file in code if file not in tests]
    for file in files:
        with open(f'{directories[1]}/{file}_test.jl', 'w') as f:
            f.writelines([])
