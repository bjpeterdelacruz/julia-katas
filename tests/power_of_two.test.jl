include("../src/power_of_two.jl")
using Test

@test ispoweroftwo(0) == false
@test ispoweroftwo(1) == true
@test ispoweroftwo(2) == true
@test ispoweroftwo(4096) == true
@test ispoweroftwo(5) == false

@test ispoweroftwo(1024) == true
@test ispoweroftwo(4096) == true
@test ispoweroftwo(333) == false
