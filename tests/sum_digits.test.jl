include("../src/sum_digits.jl")
using Test

@test sumdigits(10) == 1
@test sumdigits(99) == 18
@test sumdigits(-32) == 5

@test sumdigits(-6750131773463681165) == 80
@test sumdigits(-3188743734971032895) == 92
@test sumdigits(6483595849797518939) == 119
