include("../src/valid_spacing.jl")
using Test

@test validspacing("") == true
@test validspacing("Hello world") == true
@test validspacing(" Hello world") == false
@test validspacing("Hello  world ") == false
@test validspacing("Hello") == true
@test validspacing("Helloworld") == true
@test validspacing("Hello  world") == false
@test validspacing("a bc def ghij") == true
