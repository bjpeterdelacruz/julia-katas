include("../src/jaden_casing_strings.jl")
using Test

@test tojadencase("hello world") == "Hello World"
@test tojadencase("How can mirrors be real if our eyes aren't real") == "How Can Mirrors Be Real If Our Eyes Aren't Real"
@test tojadencase("Dying is mainstream") == "Dying Is Mainstream"
@test tojadencase("Jonah Hill is a genius") == "Jonah Hill Is A Genius"

@test tojadencase("tkqh gy ozoumxraj vz gywf") == "Tkqh Gy Ozoumxraj Vz Gywf"
