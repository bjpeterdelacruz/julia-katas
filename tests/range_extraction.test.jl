include("../src/range_extraction.jl")
using Test

@test rangeextraction([-6 -3 -2 -1 0 1 3 4 5 7 8 9 10 11 14 15 17 18 19 20]) == "-6,-3-1,3-5,7-11,14,15,17-20"
@test rangeextraction([1 3 4 5 7 8 9 10 14]) == "1,3-5,7-10,14"
@test rangeextraction([1 3 5 7 9 11]) == "1,3,5,7,9,11"
@test rangeextraction([-5 -2 -1 0 1 2]) == "-5,-2-2"
@test rangeextraction([1 2 3 4 6 8 9]) == "1-4,6,8,9"
@test rangeextraction([1 2 3 5]) == "1-3,5"
@test rangeextraction([1 2 3]) == "1-3"
@test rangeextraction([1 2]) == "1,2"
@test rangeextraction([1]) == "1"
@test rangeextraction([]) == ""

@test rangeextraction([-3 -2 -1 2 10 15 16 18 19 20]) == "-3--1,2,10,15,16,18-20"
