include("../src/negation_of_a_value.jl")
using Test

@test negationvalue("", true) == true
@test negationvalue("", false) == false
@test negationvalue("!", true) == false
@test negationvalue("!", false) == true
@test negationvalue("!!", true) == true
@test negationvalue("!!", false) == false
@test negationvalue("!!!!!", true) == false
@test negationvalue("!!!!!", false) == true
