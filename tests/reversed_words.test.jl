include("../src/reversed_words.jl")
using Test

@test reversewords("hello world!"                 ) == "world! hello"
@test reversewords("yoda doesn't speak like this" ) == "this like speak doesn't yoda"
@test reversewords("foobar"                       ) == "foobar"
@test reversewords("kata editor"                  ) == "editor kata"
@test reversewords("row row row your boat"        ) == "boat your row row row"