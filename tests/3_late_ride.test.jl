include("../src/3_late_ride.jl")
using Test

@test lateride(240) == 4
@test lateride(808) == 14
@test lateride(1439) == 19
@test lateride(0) == 0
@test lateride(23) == 5
@test lateride(8) == 8
