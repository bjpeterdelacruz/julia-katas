include("../src/hit_the_target.jl")
using Test

@test solution([
    ' ' ' ' ' ' ' '
    ' ' ' ' ' ' ' '
    ' ' '>' ' ' 'x'
    ' ' ' ' ' ' ' '
    ]) == true

@test solution([
    ' ' ' ' ' ' ' '
    ' ' '>' ' ' ' '
    ' ' ' ' ' ' 'x'
    ' ' ' ' ' ' ' '
    ]) == false

@test solution([
    ' ' ' ' ' ' ' '
    ' ' 'x' '>' ' '
    ' ' ' ' ' ' ' '
    ' ' ' ' ' ' ' '
    ]) == false

@test solution([
        '>' ' '
        ' ' 'x'
    ]) == false

@test solution([
        ' ' ' ' ' ' ' ' ' '
        ' ' ' ' ' ' ' ' ' '
        ' ' ' ' ' ' ' ' ' '
        ' ' ' ' '>' ' ' 'x'
        ' ' ' ' ' ' ' ' ' '
    ]) == true
