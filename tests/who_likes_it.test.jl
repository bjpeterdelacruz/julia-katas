include("../src/who_likes_it.jl")
using Test

@test Kata.likes(Vector{String}()) == "no one likes this"
@test Kata.likes(["Peter"]) == "Peter likes this"
@test Kata.likes(["Jacob", "Alex"]) == "Jacob and Alex like this"
@test Kata.likes(["Max", "John", "Mark"]) == "Max, John and Mark like this"
@test Kata.likes(["Alex", "Jacob", "Mark", "Max"]) == "Alex, Jacob and 2 others like this"
@test Kata.likes(["Alex", "Jacob", "Mark", "Max", "Sam"]) == "Alex, Jacob and 3 others like this"
