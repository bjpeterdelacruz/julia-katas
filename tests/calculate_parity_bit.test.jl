include("../src/calculate_parity_bit.jl")
using Test

@test checkparity("even", "101010") == 1
@test checkparity("odd", "101010") == 0
@test checkparity("even", "101011") == 0
@test checkparity("odd", "101011") == 1
