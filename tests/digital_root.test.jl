include("../src/digital_root.jl")
using Test

@test digitalroot(16) == 7
@test digitalroot(942) == 6
@test digitalroot(132189) == 6
@test digitalroot(493193) == 2

@test digitalroot(16) == 7
@test digitalroot(456) == 6
