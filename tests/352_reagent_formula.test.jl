include("../src/352_reagent_formula.jl")
using Test

@test isvalid([1,3,7]) == true
@test isvalid([7,1,2,3]) == false
@test isvalid([1,3,5,7]) == false
@test isvalid([1,5,6,7,3]) == true
@test isvalid([5,6,7]) == true
@test isvalid([5,6,7,8]) == true
@test isvalid([6,7,8]) == false
@test isvalid([7,8]) == true
