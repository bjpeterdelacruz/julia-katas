include("../src/the_old_switcheroo.jl")
using Test

@test vowel2index("this is my string") == "th3s 6s my str15ng"
@test vowel2index("THIS is my STRING") == "TH3S 6s my STR15NG"
@test vowel2index("Codewars is the best site in the world") == "C2d4w6rs 10s th15 b18st s23t25 27n th32 w35rld"
@test vowel2index("Tomorrow is going to be raining") == "T2m4rr7w 10s g1415ng t20 b23 r2627n29ng"
@test vowel2index("") == ""
