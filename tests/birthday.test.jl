include("../src/birthday.jl")
using Test

@test womensage(32) == "32? That's just 20, in base 16!"
@test womensage(39) == "39? That's just 21, in base 19!"
@test womensage(22) == "22? That's just 20, in base 11!"
@test womensage(65) == "65? That's just 21, in base 32!"
@test womensage(83) == "83? That's just 21, in base 41!"
@test womensage(63) == "63? That's just 21, in base 31!"
@test womensage(70) == "70? That's just 20, in base 35!"
@test womensage(35) == "35? That's just 21, in base 17!"
@test womensage(78) == "78? That's just 20, in base 39!"
@test womensage(34) == "34? That's just 20, in base 17!"
@test womensage(64) == "64? That's just 20, in base 32!"
@test womensage(99) == "99? That's just 21, in base 49!"
