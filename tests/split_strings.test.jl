include("../src/split_strings.jl")
using Test

pairs = solution("cdabefg")
@test pairs != nothing
@test pairs == pairs::Array
@test length(pairs) == 4
@test pairs[1]  == "cd"
@test pairs[4] == "g_"
pairs = solution("abcd")
@test length(pairs) == 2
@test pairs[2] == "cd"

@test solution("") == [""]
@test solution("a") == ["a_"]
@test solution("ab") == ["ab"]
@test solution("abc") == ["ab", "c_"]
@test solution("abcd") == ["ab", "cd"]
