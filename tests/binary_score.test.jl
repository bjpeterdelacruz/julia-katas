include("../src/binary_score.jl")
using Test

@test score(0) == 0
@test score(1) == 1
@test score(49) == 63
@test score(1000000) == 1048575
@test score(10000000) == 16777215

answer(n) = n != 0 ? 2 ^ (floor(log(2, n)) + 1) - 1 : 0

for _ in 1:50
  n = rand(1:1000)
  a = answer(n)
  @test score(n) == a
end

# for _ in 1:40
#   n = rand(1000:1000000)
#   a = answer(n)
#   @test score(n) == a
# end

# for _ in 1:10
#   n = rand(1000000:1073741824)
#   a = answer(n)
#   @test score(n) == a
# end
