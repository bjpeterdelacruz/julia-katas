include("../src/array_diff.jl")
using Test

@test arraydiff([], [4,5]) == []
@test arraydiff([3,4], [3]) == [4]
@test arraydiff([1,8,2], []) == [1,8,2]
