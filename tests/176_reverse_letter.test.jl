include("../src/176_reverse_letter.jl")
using Test

@test reverse_letter("krishan") == "nahsirk"
@test reverse_letter("ultr53o?n") == "nortlu" 
@test reverse_letter("ab23c") == "cba"
@test reverse_letter("krish21an") == "nahsirk"
