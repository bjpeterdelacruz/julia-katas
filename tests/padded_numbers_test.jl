include("../src/padded_numbers.jl")
using Test

@test solution(0) == "Value is 00000"
@test solution(5) == "Value is 00005"
@test solution(25) == "Value is 00025"
@test solution(123) == "Value is 00123"
@test solution(6789) == "Value is 06789"
