include("../src/mumbling.jl")
using Test

@test Mumbling.accum("abcd") == "A-Bb-Ccc-Dddd"
@test Mumbling.accum("abcd") == "A-Bb-Ccc-Dddd"
@test Mumbling.accum("RqaEzty") == "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
@test Mumbling.accum("cwAt") == "C-Ww-Aaa-Tttt"

@test Mumbling.accum("ZpglnRxqenU") == "Z-Pp-Ggg-Llll-Nnnnn-Rrrrrr-Xxxxxxx-Qqqqqqqq-Eeeeeeeee-Nnnnnnnnnn-Uuuuuuuuuuu"
@test Mumbling.accum("NyffsGeyylB") == "N-Yy-Fff-Ffff-Sssss-Gggggg-Eeeeeee-Yyyyyyyy-Yyyyyyyyy-Llllllllll-Bbbbbbbbbbb"
