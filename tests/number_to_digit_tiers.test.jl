include("../src/number_to_digit_tiers.jl")
using Test

@test createarrayoftiers(0) == ["0"]
@test createarrayoftiers(420) == ["4", "42", "420"]
@test createarrayoftiers(2017) == ["2", "20", "201", "2017"]
