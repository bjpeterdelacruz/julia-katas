include("../src/multiples_of_3_or_5.jl")
using Test

@test Solution.sumofmultiples(-1) == 0
@test Solution.sumofmultiples(0) == 0
@test Solution.sumofmultiples(1) == 0
@test Solution.sumofmultiples(3) == 0
@test Solution.sumofmultiples(10) == 23
@test Solution.sumofmultiples(20) == 78
