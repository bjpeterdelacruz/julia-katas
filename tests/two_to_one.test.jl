include("../src/two_to_one.jl")
using Test

@test longest("xyaabbbccccdefww", "xxxxyyyyabklmopq") == "abcdefklmopqwxy"
@test longest("abcdefghijklmnopqrstuvwxyz", "abcdefghijklmnopqrstuvwxyz") == "abcdefghijklmnopqrstuvwxyz"

@test longest("aretheyhere", "yestheyarehere") == "aehrsty"
@test longest("loopingisfunbutdangerous", "lessdangerousthancoding") == "abcdefghilnoprstu"
@test longest("inmanylanguages", "theresapairoffunctions") == "acefghilmnoprstuy"

@test longest("wwwwxxxxxfffffeeehhhhoooooocccccccpppppppiii", "qqqqqqquuuunnnnnnnmmmmjjjzzzzzzzuuuuuu") == "cefhijmnopquwxz"
@test longest("ppppppfffffffuuuuujjjjbbbbbbssstttfffjjjjjjoooocccc", "ttttttvvvvddddzzzzxxxzzzhhhhh") == "bcdfhjopstuvxz"
@test longest("wwwwwwssssssllllllccccccllljjjjjuuuuujjj", "aaaaaavvvvvvvhhhhhhpppppppdddddd") == "acdhjlpsuvw"
@test longest("llllssssssqqqqqqssssseeeeeejjjjjjvvvvvvqqqqqqqvvvvv", "jjjjjjcccppppppmmmmmmmsssssssnnnnlllllllllll") == "cejlmnpqsv"
@test longest("zzzzzzpppaaaeeeepppyyyyyyyeeeeeee", "qqqwwwwzzzxxxxhhhhuuuuu") == "aehpquwxyz"
