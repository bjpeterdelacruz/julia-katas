include("../src/convert_string_to_array.jl")
using Test

@test string_to_array("Robin Singh") == ["Robin", "Singh"]
@test string_to_array("CodeWars") == ["CodeWars"]
@test string_to_array("I love arrays they are my favorite") == ["I", "love", "arrays", "they", "are", "my", "favorite"]
@test string_to_array("1 2 3") == ["1", "2", "3"]
@test string_to_array("") == [""]

@test string_to_array("1  2  3") == ["1", "2", "3"]