include("../src/square_every_digit.jl")
using Test

@test squaredigits(9119) == 811181
@test squaredigits(7331) == 49991
