include("../src/keep_the_order.jl")
using Test

@test keeporder([1, 2, 3], 4) == 4
@test keeporder([1, 2, 3, 4, 7], 0) == 1
@test keeporder([1, 1, 2, 2, 2], 2) == 3
@test keeporder([1, 2, 3, 4], 5) == 5
@test keeporder([1, 2, 3, 4], -1) == 1
@test keeporder([1, 2, 3, 4], 2) == 2
@test keeporder([1, 2, 3, 4], 0) == 1
@test keeporder([1, 2, 3, 4], 1) == 1
@test keeporder([1, 2, 3, 4], 2) == 2
@test keeporder([1, 2, 3, 4], 3) == 3

@test keeporder(Int64[], 0) == 1
@test keeporder(Int64[], 1) == 1
@test keeporder([1, 1, 1, 1], 2) == 5
@test keeporder([1, 1, 1, 1], 1) == 1
@test keeporder([1, 1, 1, 1], 0) == 1
@test keeporder([1, 3, 5, 6], 0) == 1
@test keeporder([1, 3, 5, 6], 2) == 2
@test keeporder([1, 2, 3, 4], -1) == 1
@test keeporder([1, 2, 3, 4], -2) == 1
@test keeporder([1, 2, 3, 4], -3) == 1