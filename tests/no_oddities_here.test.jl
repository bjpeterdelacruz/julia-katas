include("../src/no_oddities_here.jl")
using Test

@test noodds([1, 2, 3, 10, 0]) == [2, 10, 0]
@test noodds([4, 5, 3]) == [4]
@test noodds([1, 5, 87, 45, 8, 8]) == [8, 8]
@test noodds([2, 0, 0, 1, 0]) == [2, 0, 0, 0]
