include("../src/which_are_in.jl")
using Test

a1 = ["live", "arp", "strong"] 
a2 = ["lively", "alive", "harp", "sharp", "armstrong"]
r = ["arp", "live", "strong"]
@test AreIn.in_array(a1, a2) == r

a1 = ["cod", "code", "wars", "ewar", "ar"] 
a2 = String[]
r = String[]
@test AreIn.in_array(a1, a2) == r

@test AreIn.in_array(["ion", "tor", "ointer", "ple", "ommo", "or",
          "oint", "he", "pinilo", "ouv", "gla", "ing", "he"],
         ["you", "reference", "would", "pointed", "I", "does",
          "for", "that", "is", "ruby-doc.", "have", "does", "you",
          "neither", "me", "opinion,", "to", "I", "have", "known",
          "1.9?", "have", "have", "not", "your", "somewhere).", "mladens", "what"]) == ["he", "ion", "oint", "or"]
