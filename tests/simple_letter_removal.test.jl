include("../src/simple_letter_removal.jl")
using Test

@test solve("abracadabra", 1) == "bracadabra"
@test solve("abracadabra", 2) == "brcadabra"
@test solve("abracadabra", 6) == "rcdbr"
@test solve("abracadabra", 8) == "rdr"
@test solve("abracadabra", 9) == "rr"
@test solve("abracadabra", 50) == ""

@test solve("sdnmeadcdwnxcojntvawzvqfbagpccesjoyqxbzhcnutnnrbwakpxxilcahbmoudnjubsh", 63) == "xzyxzxx"

@test solve("ggdhyufbgorvsosyoy", 1) == "ggdhyufgorvsosyoy"
@test solve("sj", 1) == "s"
@test solve("xlrkedhhtowmtifpbgssuttn", 2) == "xlrkehhtowmtifpgssuttn"
@test solve("hhocrz", 3) == "orz"
@test solve("ehpyauzntbpmdbolbokskrqrtypftz", 2) == "ehpyuzntpmdbolbokskrqrtypftz"
@test solve("gtqdscgseih", 5) == "tqssih"
@test solve("dvqyouxbcfsjo", 4) == "vqyouxsjo"
@test solve("g", 1) == ""
@test solve("dbadutlqzhyfoncqlrtbav", 4) == "ddutlqzhyfoncqlrtv"
@test solve("dnhnmhjqws", 3) == "nnmjqws"
