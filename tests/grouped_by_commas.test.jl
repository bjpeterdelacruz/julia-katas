include("../src/grouped_by_commas.jl")
using Test

@test groupbycommas(0) == "0"
@test groupbycommas(1) == "1"
@test groupbycommas(10) == "10"
@test groupbycommas(100) == "100"
@test groupbycommas(1000) == "1,000"
@test groupbycommas(10000) == "10,000"
@test groupbycommas(100000) == "100,000"
@test groupbycommas(1000000) == "1,000,000"
@test groupbycommas(35235235) == "35,235,235"
@test groupbycommas(2147483647) == "2,147,483,647"
