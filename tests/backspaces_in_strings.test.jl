include("../src/backspaces_in_strings.jl")
using Test

@test cleanstring("abc#d##c") == "ac"
@test cleanstring("abc##d######") == ""
@test cleanstring("#######") ==  ""
@test cleanstring("") == ""
@test cleanstring("abc#d##c") == "ac"
@test cleanstring("abc####d##c#") == ""
