include("../src/move_10.jl")
using Test

@test moveten("testcase") == "docdmkco"
@test moveten("codewars") == "mynogkbc"
@test moveten("exampletesthere") == "ohkwzvodocdrobo"

@test moveten("TESTCASE") == "DOCDMKCO"
@test moveten("codeWARS") == "mynoGKBC"
@test moveten("EXAMPLEtestHERE") == "OHKWZVOdocdROBO"
