include("../src/reverse_words.jl")
using Test

@test reversewords("The quick brown fox jumps over the lazy dog.") == "ehT kciuq nworb xof spmuj revo eht yzal .god"
@test reversewords("apple") == "elppa"
@test reversewords("a b c d") == "a b c d"
@test reversewords("double  spaced  words") == "elbuod  decaps  sdrow"

@test reversewords("  apple  ") == "  elppa  "

@test reversewords("stressed desserts") == "desserts stressed"

@test reversewords("avoid a should case always rule! is Kata hardocoded have This tests random solution. to") == "diova a dluohs esac syawla !elur si ataK dedocodrah evah sihT stset modnar .noitulos ot"
