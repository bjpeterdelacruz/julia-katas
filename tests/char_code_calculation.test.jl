include("../src/char_code_calculation.jl")
using Test

@test calc("ABC") == 6
@test calc("abcdef") == 6
@test calc("ifkhchlhfd") == 6
@test calc("aaaaaddddr") == 30
@test calc("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ") == 96
