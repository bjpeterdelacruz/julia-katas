include("../src/digitize.jl")
using Test

@test digitize(123) == [1, 2, 3]
@test digitize(1) == [1]
@test digitize(0) == [0]
@test digitize(1230) == [1, 2, 3, 0]
@test digitize(8675309) == [8, 6, 7, 5, 3, 0, 9]
