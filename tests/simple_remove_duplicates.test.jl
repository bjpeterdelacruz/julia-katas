include("../src/simple_remove_duplicates.jl")
using Test

@test solve([3, 4, 4, 3, 6, 3]) == [4, 6, 3]
@test solve([1, 2, 1, 2, 1, 2, 3]) == [1, 2, 3]
@test solve([1, 2, 3, 4]) == [1, 2, 3, 4]
@test solve([1, 1, 4, 5, 1, 2, 1]) == [4, 5, 2, 1]
