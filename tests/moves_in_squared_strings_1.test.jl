include("../src/moves_in_squared_strings_1.jl")
using Test

@test SqStr.oper(SqStr.vert_mirror, "abcd\nefgh\nijkl\nmnop") == "dcba\nhgfe\nlkji\nponm"
@test SqStr.oper(SqStr.hor_mirror, "abcd\nefgh\nijkl\nmnop") == "mnop\nijkl\nefgh\nabcd"

@test SqStr.oper(SqStr.vert_mirror, "hSgdHQ\nHnDMao\nClNNxX\niRvxxH\nbqTVvA\nwvSyRu") == "QHdgSh\noaMDnH\nXxNNlC\nHxxvRi\nAvVTqb\nuRySvw"
@test SqStr.oper(SqStr.vert_mirror, "IzOTWE\nkkbeCM\nWuzZxM\nvDddJw\njiJyHF\nPVHfSx") == "EWTOzI\nMCebkk\nMxZzuW\nwJddDv\nFHyJij\nxSfHVP"

@test SqStr.oper(SqStr.hor_mirror, "lVHt\nJVhv\nCSbg\nyeCt") == "yeCt\nCSbg\nJVhv\nlVHt"
