include("../src/two_oldest_ages.jl")
using Test

@test twooldestages([1, 3, 10, 0]) == [3, 10]
@test twooldestages([4, 5, 3]) == [4, 5]
@test twooldestages([1, 5, 87, 45, 8, 8]) == [45, 87]
@test twooldestages([2, 0, 0, 1, 0]) == [1, 2]
