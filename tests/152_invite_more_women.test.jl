include("../src/152_invite_more_women.jl")
using Test

@test invitemorewomen([1, -1, 1]) == true
@test invitemorewomen([1, 1, 1]) == true
@test invitemorewomen([-1, -1, -1]) == false
@test invitemorewomen([1, -1]) == false

@test invitemorewomen([1, 1, 1, 1, 1, -1, -1, -1, 1, -1, -1, 1, 1, 1, -1,
                        1, 1, 1, -1, 1, -1, 1, 1, 1, -1, 1, -1, -1, 1, -1, 1, 1, -1, 1, 1, 1, 1, -1, -1, 1, -1, -1]) == true
