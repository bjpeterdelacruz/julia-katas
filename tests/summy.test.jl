include("../src/summy.jl")
using Test

@test summy("1 2 3") == 6
@test summy("1 2 3 4") == 10
@test summy("1 2 3 4 5") == 15
@test summy("10 10") == 20
@test summy("0 0") == 0
