include("../src/37_house_numbers_sum.jl")
using Test

@test housenumberssum([5, 1, 2, 3, 0, 1, 5, 0, 2]) == 11
@test housenumberssum([4, 2, 1, 6, 0]) == 13
@test housenumberssum([4, 1, 2, 3, 0, 10, 2]) == 10
@test housenumberssum([0, 1, 2, 3, 4, 5]) == 0
