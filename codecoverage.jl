using Coverage
coverage = process_folder()
covered_lines, total_lines = get_summary(coverage)
# LCOV.writefile("coverage-lcov.info", coverage)
clean_folder("src")

println("Covered lines: ", covered_lines)
println("Total lines: ", total_lines)
