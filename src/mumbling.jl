module Mumbling
    export accum

    function accum(s)
        arr = []
        idx = 1
        for c in s
            push!(arr, string(uppercase(c), repeat(lowercase(c), idx - 1)))
            idx += 1
        end
        join(arr, "-")
    end

end
