function longest(a1, a2)
    characters = Set(collect(a1))
    for char in a2
        push!(characters, char)
    end
    return join(sort(collect(characters)))
end
