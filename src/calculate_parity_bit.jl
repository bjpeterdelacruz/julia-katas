function checkparity(parity, bin)
    count = sum([1 for digit in bin if digit == '1'])
    if parity == "even" && count % 2 == 1
        return 1
    elseif parity == "odd" && count % 2 == 0
        return 1
    end
    return 0
end
