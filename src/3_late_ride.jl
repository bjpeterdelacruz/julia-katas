function lateride(n)
    sum([parse(Int, "$(x)") for x in string(n ÷ 60, mod(n, 60))])
end
