function squaredigits(num)
    return parse(Int, join([parse(Int, "$x") * parse(Int, "$x") for x in string(num)], ""))
end
