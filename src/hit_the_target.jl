function solution(A::Matrix{Char})::Bool
    dim = size(A)[1]
    arr_idx = -1
    x_idx = -1
    for idx in eachindex(A)
        if A[idx] == '>'
            arr_idx = idx
        elseif A[idx] == 'x'
            x_idx = idx
        end
        if arr_idx != -1 && x_idx != -1
            break
        end
    end
    if arr_idx > x_idx || arr_idx == -1 || x_idx == -1
        # arrow flew past target, arrow or target not found
        return false
    end
    current = x_idx
    while current > 0
        if current == arr_idx
            # arrow is on same row as target
            return true
        end
        current -= dim
    end
    # arrow is not on same row as target
    return false
end
