function housenumberssum(inputarray)
    sum = 0
    for num in inputarray
        if num == 0
            return sum
        else
            sum += num
        end
    end
end
