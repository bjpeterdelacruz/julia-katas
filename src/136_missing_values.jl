function missingvalues(ns::Vector{Int})::Int
    counts = Dict{Int, Int}()
    for num in ns
        if num in keys(counts)
            counts[num] += 1
        else
            counts[num] = 1
        end
    end
    x = NaN
    y = NaN
    for (key, value) in counts
        if value == 2
            y = key
        elseif value == 1
            x = key
        end
        if !isnan(x) && !isnan(y)
            return x * x * y
        end
    end
end
