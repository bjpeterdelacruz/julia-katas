function digitize(n)
    [parse(Int32, char) for char in string(n)]
end
