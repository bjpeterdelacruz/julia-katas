function solve(a)
    result = []
    unique_numbers = Set()
    for idx in Iterators.reverse(1:length(a))
        num = a[idx]
        if num ∉ unique_numbers
            push!(result, num)
            push!(unique_numbers, num)
        end
    end
    return reverse(result)
end
