function isvalid(formula)
    if (indexin(1, formula) .!= nothing && indexin(2, formula) .!= nothing) ||
        (indexin(3, formula) .!= nothing && indexin(4, formula) .!= nothing)
        return false
    end
    if (indexin(5, formula) .!= nothing && indexin(6, formula) .== nothing) ||
        (indexin(5, formula) .== nothing && indexin(6, formula) .!= nothing)
        return false
    end
    return indexin(7, formula) .!= nothing || indexin(8, formula) .!= nothing
end
