function moveten(s::AbstractString)::String
    ascii_upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    ascii_lower = "abcdefghijklmnopqrstuvwxyz"
    ns = []
    for char in s
        if isletter(char)
            idx = findfirst(isequal(char), ascii_lower)
            if idx == nothing
                idx = findfirst(isequal(char), ascii_upper)
                idx = idx + 10 == 26 ? 26 : mod(idx + 10, 26)
                push!(ns, ascii_upper[idx])
            else
                idx = idx + 10 == 26 ? 26 : mod(idx + 10, 26)
                push!(ns, ascii_lower[idx])
            end
        else
            push!(ns, char)
        end
    end
    join(ns, "")
end
