function twooldestages(ages)
    oldest_age = 0
    second_oldest_age = 0
    for age in ages
        if age > oldest_age
            second_oldest_age = oldest_age
            oldest_age = age
        elseif age > second_oldest_age
            second_oldest_age = age
        end
    end
    return [second_oldest_age, oldest_age]
end
