function digitalroot(n)
    total = sum([parse(Int, "$(digit)") for digit in string(n)])
    while length(string(total)) > 1
        total = sum([parse(Int, "$(digit)") for digit in string(total)])
    end
    total
end
