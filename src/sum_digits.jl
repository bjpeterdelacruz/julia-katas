function sumdigits(number)
    sum([parse(Int, "$(digit)") for digit in string(number) if isdigit(digit)])
end
