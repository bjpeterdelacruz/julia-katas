function noodds(values::Array{Int,1})::Array{Int,1}
    [value for value in values if mod(value, 2) == 0]
end
