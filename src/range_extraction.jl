function rangeextraction(list)
    if length(list) == 0
        return ""
    elseif length(list) == 1
        return "$(list[end])"
    end
    last = nothing
    results = []
    index = 2
    while index <= length(list)
        difference = abs(list[index - 1] - list[index])
        if difference == 1
            temp = []
            while difference == 1
                push!(temp, list[index - 1])
                index += 1
                if index > length(list)
                    break
                end
                difference = abs(list[index - 1] - list[index])
            end
            push!(temp, list[index - 1])
            if length(temp) > 2
                push!(results, "$(temp[1])-$(temp[end])")
            else
                push!(results, temp[1])
                push!(results, temp[2])
            end
            index -= 1
            last = temp[end]
        elseif last != list[index - 1]
            push!(results, list[index - 1])
        elseif index == length(list) && last != list[end]
            # add list[end] to results if range came before it
            last = list[end]
            push!(results, list[end])
        end
        index += 1
    end
    if last != list[end]
        # add list[end] to results if no range came before it
        push!(results, list[end])
    end
    return join(results, ",")
end
