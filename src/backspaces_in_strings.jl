function cleanstring(s)
    arr = []
    for c in s
        if c == '#' && length(arr) > 0
            pop!(arr)
        elseif c != '#'
            push!(arr, c)
        end
    end
    join(arr, "")
end
