function validspacing(s::String)::Bool
    if length(s) == 0
        return true
    end
    occursin(r"^\w+( \w+)*$", s)
end