function fakebin(x)
    join(map(letter -> parse(Int, letter) < 5 ? "0" : "1", split(x, "")), "")
end
