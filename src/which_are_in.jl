module AreIn
    export in_array

    function in_array(array1, array2)
        results = []
        for element1 in array1
            for element2 in array2
                if occursin(element1, element2)
                    push!(results, element1)
                    break
                end
            end
        end
        sort([elem for elem in Set(results)])
    end

end