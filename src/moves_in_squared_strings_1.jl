module SqStr
    export vert_mirror, hor_mirror, oper

    function vert_mirror(s)
        return join([reverse(st) for st in s], "\n")
    end
    function hor_mirror(s)
        return join(reverse(s), "\n")
    end
    function oper(func, s)
        return func(split(s, "\n"))
    end
end
