function summy(stringofints::String)::Int
    sum(map(x -> parse(Int, x), split(stringofints)))
end
