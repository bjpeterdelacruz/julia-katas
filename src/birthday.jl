function womensage(number::Int)::String
    base = 2
    result = number
    while true
        temp = []
        while result ÷ base != 0
            insert!(temp, 1, rem(result, base))
            result = result ÷ base
        end
        insert!(temp, 1, rem(result, base))
        new_age = parse(Int, join(temp))
        if new_age == 20 || new_age == 21
            return "$(number)? That's just $(new_age), in base $(base)!"
        end
        base += 1
        result = number
    end
end
