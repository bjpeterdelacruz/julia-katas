function tojadencase(str)
    newstr = [uppercase(str[1])]
    idx = 1
    for idx in 2:length(str)
        push!(newstr, isletter(str[idx]) && isspace(str[idx - 1]) ? uppercase(str[idx]) : str[idx])
        idx += 1
    end
    join(newstr, "")
end
