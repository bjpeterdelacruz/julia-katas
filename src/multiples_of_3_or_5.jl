module Solution
    export sumofmultiples

    function createmultiples(multiple, number)
        i = 1
        set = Set()
        while multiple * i < number
            push!(set, multiple * i)
            i += 1
        end
        set
    end

    function sumofmultiples(number)
        if number <= 3
            return 0
        end
        sum(union(createmultiples(3, number), createmultiples(5, number)))
    end
end
