function vowel2index(str::String)::String
    join([str[index] in  "aeiouAEIOU" ? index : str[index] for index in 1:length(str)], "")
end
