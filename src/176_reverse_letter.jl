function reverse_letter(string)
    return join([s for s in reverse(string) if isletter(s)], "")
end
