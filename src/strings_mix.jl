function customsort(tup1, tup2)
    if tup1[2] == tup2[2]
        if tup1[3] == tup2[3]
            return tup1[1] > tup2[1]
        end
        return tup1[3] > tup2[3]
    end
    return tup1[2] < tup2[2]
end

function mix(s1, s2)
    letters = "abcdefghijklmnopqrstuvwxyz"

    counts_s1 = Dict{Char, Int}()
    counts_s2 = Dict{Char, Int}()

    for letter in letters
        counts_s1[letter] = 0
        counts_s2[letter] = 0
    end

    for letter in s1
        if haskey(counts_s1, letter)
            counts_s1[letter] += 1
        end
    end
    for letter in s2
        if haskey(counts_s2, letter)
            counts_s2[letter] += 1
        end
    end

    all_counts = []
    for (key, value) in counts_s1
        if haskey(counts_s2, key) && counts_s2[key] > 1
            if counts_s2[key] > value
                push!(all_counts, (key, counts_s2[key], '2'))
                continue
            elseif counts_s2[key] == value
                push!(all_counts, (key, counts_s2[key], '='))
                continue
            end
        end
        if value > 1
            push!(all_counts, (key, value, '1'))
        end
    end

    for (key, value) in counts_s2
        if !haskey(counts_s1, key) && value > 1
            push!(all_counts, (key, value, '2'))
        end
    end

    sort!(all_counts, lt = customsort, rev = true)
    arr = []
    for count in all_counts
        push!(arr, count[3] * ":" * (count[1] ^ count[2]))
    end
    return join(arr, "/")
end
