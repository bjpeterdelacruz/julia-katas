function reversewords(str)
    reversedwords = []
    newstr = []
    for idx in 1:length(str)
        if isspace(str[idx])
            if length(newstr) > 0
                push!(reversedwords, reverse(join(newstr, "")))
            end
            newstr = []
            push!(reversedwords, str[idx])
        else
            push!(newstr, str[idx])
        end
    end
    push!(reversedwords, reverse(join(newstr, "")))
    join(reversedwords, "")
end
