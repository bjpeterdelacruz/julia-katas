function createarrayoftiers(num::Integer)::Vector{String}
    chars = string(num)
    arr = []
    push!(arr, "$(chars[1])")
    for idx in 2:length(chars)
        push!(arr, string(arr[end], "$(chars[idx])"))
    end
    arr
end
