function solution(str)
    if str == ""
        return [""]
    end
    s = str
    if mod(length(str), 2) == 1
        s = string(str, "_")
    end
    arr = []
    for idx in 1:length(s)
        if mod(idx, 2) == 0
            push!(arr, s[idx - 1:idx])
        end
    end
    arr
end
