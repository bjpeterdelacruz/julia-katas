function calc(x::String)::Int
    total1 = join([Int(s) for s in x], "")
    total2 = join([s == '7' ? '1' : s for s in total1], "")
    sum([parse(Int, "$s") for s in total1]) - sum([parse(Int, "$s") for s in total2])
end
