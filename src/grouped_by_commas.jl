function groupbycommas(n)
    s = reverse(string(n))
    arr = []
    for idx in 1:length(s)
        push!(arr, s[idx])
        if mod(idx, 3) == 0 && idx != length(s)
            push!(arr, ',')
        end
    end
    reverse(join(arr, ""))
end
