function gordon(ramsay::AbstractString)::String
    a = "aA"
    vowels = "eiouEIOU"
    arr = []
    word = []
    for c in ramsay
        if isspace(c)
            if length(word) > 0
                push!(arr, string(join(word, ""), "!!!!"))
                word = []
            end
        elseif c in a
            push!(word, "@")
        elseif c in vowels
            push!(word, "*")
        else
            push!(word, uppercase(c))
        end
    end
    if length(word) > 0
        push!(arr, string(join(word, ""), "!!!!"))
    end
    join(arr, " ")
end
