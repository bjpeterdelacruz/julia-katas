letters = "abcdefghijklmnopqrstuvwxyz"

function solve(s::String, k::Int)::String
    count = k
    if length(s) <= count
        return ""
    end
    for letter in letters
        while occursin(letter, s)
            s = replace(s, "$(letter)" => "", count=1)
            count -= 1
            if count == 0
                return s
            end
        end
    end
end
